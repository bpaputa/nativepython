import sys
import py_compile
import marshal
import dis
import types

#assert len(sys.argv) >= 2
#filename = sys.argv[1]

filename = ""
with open("filename") as f:
	filename = f.readline()[:-1]


with open("_" + filename, "w") as f1:
	with open("runtime.py") as f:
		for line in f:
			f1.write(line) 		
	with open(filename) as f:
		for line in f:
			f1.write(line) 

py_compile.compile("_" + filename)
f = open("__pycache__/_" + filename[:-2] + "cpython-37.pyc", "rb")

f.read(4) 
f.read(4) 
f.read(4) 

code = marshal.load(f)

def show_code(code, indent=''):
	print("%scode" % indent)
	indent += '   '
	print("%sargcount %d" % (indent, code.co_argcount))
	print("%snlocals %d" % (indent, code.co_nlocals))
	print("%sstacksize %d" % (indent, code.co_stacksize))
	print("%sflags %04x" % (indent, code.co_flags))
	show_hex("bytecode", code.co_code, indent=indent)
	dis.disassemble(code)
	print("%sconsts" % indent)
	for const in code.co_consts:
		if type(const) == types.CodeType:
			show_code(const, indent+'   ')
		else:
			print("   %s%r" % (indent, const))
	print("%snames %r" % (indent, code.co_names))
	print("%svarnames %r" % (indent, code.co_varnames))
	print("%sfreevars %r" % (indent, code.co_freevars))
	print("%scellvars %r" % (indent, code.co_cellvars))
	print("%sfilename %r" % (indent, code.co_filename))
	print("%sname %r" % (indent, code.co_name))
	print("%sfirstlineno %d" % (indent, code.co_firstlineno))
	show_hex("lnotab", code.co_lnotab, indent=indent)
	print("\n")
	
def show_hex(label, h, indent):
	h = h.hex()
	print("%s%s" % (indent, label), end='')
	for i in range(0, len(h), 2):
		if i % 16 == 0:
			print("\n%s   " % indent, end='')
		print(h[i], end = '')
		print(h[i+1], end = ' ')
	print()
#	if len(h) < 60:
#		print("%s%s %s" % (indent, label, h))
#	else:
#		print("%s%s" % (indent, label))
#		for i in range(0, len(h), 60):
#			print("%s   %s" % (indent, h[i:i+60]))

def simpleShowHex(code):
	code = code.hex()
	print(int(len(code)/2))
	for i in range(0, len(code), 2):
		print(code[i], end = '')
		print(code[i+1], end = ' ')
	print()

def printConst(x):
	print(type(x).__name__)
	if type(x) == types.CodeType:
		simpleShowCode(x)
	elif type(x) == str:
		print(str(x), end="\0\n")
	elif type(x) == tuple:
		print(len(x))
		for c in x:
			printConst(c)
	else:
		print(str(x), end="\n")

def simpleShowCode(code):
	#print("%s %d %d\n" % ( code.co_name, code.co_argcount, code.co_stacksize), end='')
	print("%s %d\n" % ( code.co_name, code.co_argcount), end='')
	simpleShowHex(code.co_code)
	
	#print("globals")
	print(len(code.co_names))
	for x in code.co_names:
		print(x, end = " ")
	if len(code.co_names):
		print()
	
	#print("locals")
	print(len(code.co_varnames))
	for x in code.co_varnames:
		print(x, end = " ")
	if len(code.co_varnames):
		print()
	
	#print("consts")
	print(len(code.co_consts))
	for x in code.co_consts:
		printConst(x)
		
if len(sys.argv) == 2:
	simpleShowCode(code)	
else:
	show_code(code)
