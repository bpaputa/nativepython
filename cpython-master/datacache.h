#ifndef DATACACHE_H
#define DATACACHE_H

#include<stdlib.h>
#include<math.h>

enum blockState{INVALID, VALID, DIRTY};
typedef enum blockState blockState_t;

struct block{
    int tag;
    int mru;
    blockState_t state;
};
typedef struct block block_t;

struct set{
    block_t* blocks;
};
typedef struct set set_t;

set_t* sets;
int blockSize, numberOfSets, blocksPerSet, size, setSize;

enum cacheResult{DCACHE_HIT, DCACHE_MISS};
typedef enum cacheResult cacheResult_t;

void initDCache(){
	blockSize = 4;
    numberOfSets = 512;
    blocksPerSet = 2;
    setSize = blockSize * blocksPerSet;
    size = setSize * numberOfSets;
    
    sets = calloc(numberOfSets, sizeof(set_t));
    for(int i = 0; i < numberOfSets; i++){
        sets[i].blocks = calloc(blocksPerSet, sizeof(block_t));
        for(int j = 0; j < blocksPerSet; j++){
            sets[i].blocks[j].state = INVALID;
            sets[i].blocks[j].mru = -1;
        }
    }
}

int usecount = 0;
enum action{READ, WRITE};
typedef enum action action_t;

cacheResult_t operation(action_t action, int addr){
    int setidx = (addr >> (int)log2(setSize)) & (numberOfSets-1);
    set_t* set = sets + setidx;
    int tag = addr - addr%blockSize,
        blockOffset = addr & (blockSize-1),
        lowestMRU = 2147483647;
    block_t* lowestMRUblock = NULL;
    for(int i = 0; i < blocksPerSet; i++){
        block_t* block = set->blocks + i;
        if(block->state != INVALID && block->tag == tag){
            block->mru = usecount++;
            if(action == WRITE){
            	block->state = DIRTY;
            }
            return DCACHE_HIT;
        } else if(block->mru < lowestMRU){
            lowestMRU = block->mru;
            lowestMRUblock = block;
        }
    }
    lowestMRUblock->tag = tag;
    lowestMRUblock->state = VALID;
    lowestMRUblock->mru = usecount++;
    return DCACHE_MISS;
}

#endif
