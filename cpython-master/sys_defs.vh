
`ifndef SYS_DEFS_VH
`define SYS_DEFS_VH

//`define SD #1
`define SD
`define VERILOG_CLOCK_PERIOD 15

`define STACK_SIZE 64
`define SP_SIZE 6

`define BYTECODE_SIZE 512
`define BYTECODE_PTR_SIZE 9

`define CONSTS_SIZE 128
`define CONSTS_PTR_SIZE 7

`define NAMES_SIZE 64
`define NAMES_PTR_SIZE 6

`define STRINGS_SIZE 128
`define STRINGS_PTR_SIZE 7

//`define TYPES_SIZE 64

`define GLOBALS_SIZE 64
`define GLOBALS_PTR_SIZE 6

`define CALL_STACK_LIMIT 32
`define CALL_SP_SIZE 5

`define CODE_OBJECTS_SIZE 32
`define CODE_OBJECTS_PTR_SIZE 5

`define NUM_LISTS 32
`define LIST_PTR_SIZE 5

`define LIST_HEAP_SIZE 128
`define LIST_HEAP_PTR_SIZE 7



//`define NUM_METHODS 64


typedef enum logic [2:0] {
	NONE	= 3'h0,
	BOOL	= 3'h1,
	INT		= 3'h2,
	STRING	= 3'h3,
	CODE	= 3'h4,
	LIST	= 3'h5, 
	TUPLE	= 3'h6, 
	ITER	= 3'h7
} ObjectType;

typedef struct packed {
	logic [31:0]	data;
	ObjectType	 	t;
} Object;

`define MAX_ARGS 		16
`define ARGCOUNT_SIZE 	4

`define MAX_LOCALS		32
`define LOCALS_SIZE		5

`define MAX_CONSTS		16
`define CO_CONSTS_SIZE	4

`define MAX_NAMES		16
`define CO_NAMES_SIZE	4

typedef struct packed {
	logic [`ARGCOUNT_SIZE-1:0] 		argcount;
	logic [`LOCALS_SIZE-1:0]		nlocals;
	logic [`BYTECODE_PTR_SIZE-1:0]	bytecodeStart;
	logic [`CONSTS_PTR_SIZE-1:0]	constStart;
	logic [`NAMES_PTR_SIZE-1:0]		nameStart;
	logic [`CO_CONSTS_SIZE-1:0]		nconsts;
	logic [`CO_NAMES_SIZE-1:0]		nnames;
} CodeObject;

typedef struct packed {
	logic [`CODE_OBJECTS_PTR_SIZE-1:0]	code;
	logic [`BYTECODE_PTR_SIZE-1:0]		returnAddress;
	logic [`SP_SIZE-1:0]				prevsp;
} FrameObject;

typedef enum logic[1:0] {
	FUNCTION = 2'h0,
	METHOD	 = 2'h1,
	GLOBAL 	 = 2'h2,
	LOCAL    = 2'h3
} NameType;

typedef struct packed {
	NameType t;
	logic [`CODE_OBJECTS_PTR_SIZE-1:0] index;
} Name;


typedef logic[7:0] Bytecode;

typedef logic[6:0] Char;

typedef logic[`LIST_HEAP_PTR_SIZE-1:0] ListHeapPtr;

typedef logic[`LIST_HEAP_PTR_SIZE:0] ListSize;

typedef enum Bytecode {
	POP_TOP	= 1,
	DUP_TOP	= 4,
	UNARY_NOT	= 12,
	BINARY_MULTIPLY	= 20,
	BINARY_MODULO	= 22,
	BINARY_ADD	= 23,
	BINARY_SUBTRACT	= 24,
	BINARY_SUBSCR	= 25,
	BINARY_TRUE_DIVIDE	= 27,
	INPLACE_MODULO 	= 54,
	INPLACE_ADD 	= 55,
	INPLACE_SUBTRACT= 56,
	INPLACE_MULTIPLY	= 57,	
	STORE_SUBSCR	= 60,
	GET_ITER	= 68,
	BREAK_LOOP	= 80,
	RETURN_VALUE	= 83,
	POP_BLOCK	= 87,
	STORE_NAME 	= 90,
	UNPACK_SEQUENCE	= 92,
	FOR_ITER	= 93,
	STORE_ATTR	= 95,
	STORE_GLOBAL	= 97,
	LOAD_CONST	= 100,
	LOAD_NAME 	= 101,
	BUILD_TUPLE	= 102,
	BUILD_LIST	= 103,
	LOAD_ATTR	= 106,
	COMPARE_OP	= 107,
	JUMP_FORWARD	= 110,
	JUMP_IF_TRUE_OR_POP	= 112,
	JUMP_ABSOLUTE	= 113,
	POP_JUMP_IF_FALSE	= 114,
	POP_JUMP_IF_TRUE	= 115,
	LOAD_GLOBAL	= 116,
	SETUP_LOOP	= 120,
	LOAD_FAST	= 124,
	STORE_FAST	= 125,
	CALL_FUNCTION	= 131,
	MAKE_FUNCTION 	= 132,
	EXTENDED_ARG	= 145,
	LOAD_METHOD	= 160,
	CALL_METHOD	= 161,
	LOAD_FAST_LEN = 251,
	LOAD_FAST_TYPE = 252,
	BUILD_RANGE = 253,
	DECL_LIST = 254,
	PRINT = 255
} Opcodes;

typedef enum logic[3:0] {
	LT,
	LTE,
	EQ,
	NEQ,
	GT,
	GTE,
	IN,
	NOTIN,
	IS,
	ISNOT,
	EXCEPTIONMATCH,
	BAD
} CmpOps;

typedef logic garbage;


//	`define ERROR(m) do begin end while(0)

	`define ERROR(m) do begin $write("[%0t] %s:%0d (ir=%0d): %s\n", $time, `__FILE__, `__LINE__, ir, $sformatf m); end while(0)


`endif

