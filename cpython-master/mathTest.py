
def fib(n):
	x = 0
	if n == 0 or n == 1:
		x = 1
	else:
		x = fib(n-1) + fib(n-2)

	return x

def fact(n):
	if n:
		return n * fact(n-1)

	return 1

def main():
	testNum = 10

	print('\n')
	print("Fibonacci Number of ")
	print(testNum)
	print(":\n")
	print(fib(10))
	print("\n")
	print("Factorial of ")
	print(testNum)
	print(":\n")
	print(fact(10))
	print('\n\n')

main()
