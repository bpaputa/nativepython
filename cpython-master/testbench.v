
extern int getStack_Obj_data(int i);
extern int getStack_Obj_type(int i);
extern int getStack_Obj_numRefs(int i);

//Get LocalsStack Data
extern int getLocStack_Obj_data(int i);
extern int getLocStack_Obj_type(int i);
extern int getLocStack_Obj_numRefs(int i);

//Get Bytecode Data
extern int getBytecode(int i);

//Get CodeObject Data
extern int getCO_ArgCount(int i);
extern int getCO_NLocals(int i);
extern int getCO_BytecodeStart(int i);
extern int getCO_ConstStart(int i);
extern int getCO_NameStart(int i);
extern int getCO_NumConsts(int i);
extern int getCO_NumNames(int i);

//Get FrameObject Data
extern int getFO_CodeObjectPtr(int i);
extern int getFO_BytecodePtr(int i);
extern int getFO_prevsp(int i);

//Get consts Data
extern int getConsts_Obj_data(int i);
extern int getConsts_Obj_type(int i);
extern int getConsts_Obj_numRefs(int i);

//Get globals Data
extern int getGlobals_Obj_data(int i);
extern int getGlobals_Obj_type(int i);
extern int getGlobals_Obj_numRefs(int i); 

//Get Name Data
extern int getName_type(int i);
extern int getName_index(int i);

//Get strings Data
extern int getString(int i);

//Get listHeap Data
extern int getListHeap_Obj_data(int i); 
extern int getListHeap_Obj_type(int i);
extern int getListHeap_Obj_numRefs(int i);

//Get listStarts Data
extern int getListStarts(int i);
//Get listSizes Data
extern int getListSizes(int i);

extern int getNextList();
extern int getListHeapPtr();




`timescale 1ns/100ps
`include "sys_defs.vh"

//`include "cpython-master/sim.h"

extern void init();

module testbench();
	
	logic clk;
	logic reset;
	
	Bytecode[`BYTECODE_SIZE-1:0] 		reset_bytecode;
	CodeObject[`CODE_OBJECTS_SIZE-1:0]	reset_codeObjects;
	Object[`CONSTS_SIZE-1:0] 			reset_consts;
	Object[`GLOBALS_SIZE-1:0] 			reset_globals;
	Name[`NAMES_SIZE-1:0]	 			reset_names;	
	Char[`STRINGS_SIZE-1:0] 			reset_strings;
	Object[`LIST_HEAP_SIZE-1:0] 		reset_listHeap;
	ListHeapPtr[`NUM_LISTS-1:0] 		reset_listStarts;
	ListSize[`NUM_LISTS-1:0] 			reset_listSizes;
	logic[`LIST_PTR_SIZE-1:0] 			reset_nextList;
	ListHeapPtr 						reset_listHeapPtr;
	
	logic error;
	logic halt;
	
	logic doPrint;
	Object printData;
	
	Object[`STACK_SIZE-1:0] stack, localStack;
	logic[`SP_SIZE-1:0]	sp, localsp;
	
	logic[`BYTECODE_PTR_SIZE-1:0] ir;
	logic[7:0] opcode, arg;
	
	FrameObject[`CALL_STACK_LIMIT-1:0]	frameStack;
	logic [`CALL_SP_SIZE-1:0]			fp;
	
	
	Object[`LIST_HEAP_SIZE-1:0] listHeap;
	ListHeapPtr[`NUM_LISTS-1:0] listStarts;
	ListSize[`NUM_LISTS-1:0] 	listSizes; 
	logic[`LIST_PTR_SIZE-1:0] 	nextList;
	ListHeapPtr 				listHeapPtr;
	
	Object[`GLOBALS_SIZE-1:0] globals;
	
	logic[31:0] cycleCount;
	
	pipeline u(
		.clk(clk),
		.reset(reset),
	
		.reset_bytecode(reset_bytecode),
		.reset_codeObjects(reset_codeObjects),
		.reset_consts(reset_consts),
		.reset_globals(reset_globals),
		.reset_names(reset_names),	
		.reset_strings(reset_strings),
		.reset_listHeap(reset_listHeap),
		.reset_listStarts(reset_listStarts),
		.reset_listSizes(reset_listSizes),
		.reset_nextList(reset_nextList),
		.reset_listHeapPtr(reset_listHeapPtr),
	
		.error(error),
		.halt(halt),
	
		.doPrint(doPrint),
		.printData(printData),
		
		.stack(stack),
		.sp(sp),
		.localStack(localStack),
		.localsp(localsp),
		
		.ir(ir),
		
		.opcode(opcode),
		.arg(arg),
		
		.frameStack(frameStack),
		.fp(fp),
		
		.listHeap(listHeap),
		.listStarts(listStarts),
		.listSizes(listSizes),
		.nextList(nextList),
		.listHeapPtr(listHeapPtr),
		
		.globals(globals)
	);
	
	
	task printObj;
		input Object obj;
		input Char[`STRINGS_SIZE-1:0] reset_strings;
		begin
			case(obj.t) 
				NONE:
					$write("None(%0d)", obj.data);
				BOOL:
					$write(obj.data ? "True" : "False");
				INT:
					$write("%0d", obj.data);
				STRING: begin
					//$write("\"");
					for(int i = obj.data; reset_strings[i] != 0; i++) begin
						$write("%c", reset_strings[i]);
					end
				end
				CODE:
					$write("<CodeObject #%0d>", obj.data);
				3'bXXX:
					$display("XXX");
				LIST:
					$write("<List #%0d>", obj.data);
				TUPLE:
					$write("<Tuple #%0d>", obj.data);			
				ITER:				
					$write("<Iter #%0d>", obj.data);
				default: begin
					$display("@@@ Illegal type printed: %d", obj.t);
					$finish;
				end
			endcase
		end
	endtask
	
	always begin
		#(`VERILOG_CLOCK_PERIOD/2.0);
		clk = ~clk;
		cycleCount++;
	end
	
	int noneCount;
	initial begin
		init();
		for(int i = 0; i < `BYTECODE_SIZE; i++) begin
			reset_bytecode[i] = (getBytecode(i));//[7:0];
		end
		for(int i = 0; i < `CODE_OBJECTS_SIZE; i++) begin
			reset_codeObjects[i].argcount = getCO_ArgCount(i);//[`ARGCOUNT_SIZE-1:0];
			reset_codeObjects[i].nlocals = getCO_NLocals(i);//[`LOCALS_SIZE-1:0];
			reset_codeObjects[i].bytecodeStart = getCO_BytecodeStart(i);//[`BYTECODE_PTR_SIZE-1:0];
			reset_codeObjects[i].constStart = getCO_ConstStart(i);//[`CONSTS_PTR_SIZE-1:0];
			reset_codeObjects[i].nameStart = getCO_NameStart(i);//[`NAMES_PTR_SIZE-1:0];
			reset_codeObjects[i].nconsts = getCO_NumConsts(i);//[`CO_CONSTS_SIZE-1:0];
			reset_codeObjects[i].nnames = getCO_NumNames(i);//[`CO_NAMES_SIZE-1:0];
		end
		noneCount = 0;
		$display("Strings: ");
		for(int i = 0; i < `STRINGS_SIZE; i++) begin
			reset_strings[i] = getString(i);//[6:0];
			if(reset_strings[i] == 0) begin
				noneCount++;
			end else begin
				noneCount = 0;
			end
			if(noneCount <= 3) begin
				$display("\t%d: %c (%0d)", i, reset_strings[i], reset_strings[i]); 
			end
		end
		noneCount = 0;
		$display("Consts: ");
		for(int i = 0; i < `CONSTS_SIZE; i++) begin
			reset_consts[i].data = getConsts_Obj_data(i);
			reset_consts[i].t = ObjectType'(getConsts_Obj_type(i));//[2:0];
			if(reset_consts[i].t == NONE) begin
				noneCount++;
			end else begin
				noneCount = 0;
			end
			if(noneCount <= 3) begin
				$write("\t%d:", i); printObj(reset_consts[i], reset_strings); $display("");
			end
		end
		for(int i = 0; i < `GLOBALS_SIZE; i++) begin
			reset_globals[i].data = getGlobals_Obj_data(i);
			reset_globals[i].t = ObjectType'(getGlobals_Obj_type(i));//[2:0];
		end
		noneCount = 0;
		$display("Names: ");
		for(int i = 0; i < `NAMES_SIZE; i++) begin
			reset_names[i].t = NameType'(getName_type(i));//[1:0];
			reset_names[i].index = getName_index(i);//[`CODE_OBJECTS_PTR_SIZE-1:0];
			if(reset_consts[i].t == NONE) begin
				noneCount++;
			end else begin
				noneCount = 0;
			end
			if(noneCount <= 3) begin
				$display("\t%0d:%0d", i, reset_names[i]);
			end
		end
		
		for(int i = 0; i < `LIST_HEAP_SIZE; i++) begin
			reset_listHeap[i].data = getListHeap_Obj_data(i);
			reset_listHeap[i].t = ObjectType'(getListHeap_Obj_type(i));
		end
		for(int i = 0; i < `NUM_LISTS; i++) begin
			reset_listSizes[i] = getListSizes(i);
			reset_listStarts[i] = getListStarts(i);
		end
		reset_listHeapPtr = getListHeapPtr();
		reset_nextList = getNextList();
		
		
		reset = 1;
		clk = 0;
		#15
		clk = 1;
		#15
		reset = 0;
		cycleCount = 0;
	end
					
	
	task printState;
		begin
			$display("@@@ CYCLE %d:\n\tsp: %d\n\tstack:", cycleCount, sp);
			if(sp != 255) begin
				noneCount = 0;
				for(int i = 0; i <= sp; i++) begin
					if(stack[i].t == NONE) begin
						noneCount++;
					end else begin
						noneCount = 0;
					end 
					if(noneCount <= 3) begin
						$write("\t\t%0d:", i); printObj(stack[i], reset_strings); $display("");
					end
				end
			end
			$display("\tlocalsp: %d\n\tlocalStack:", localsp);
			noneCount = 0;
			for(int i = 0; i <= `LOCALS_SIZE; i++) begin
				if(localStack[i].t == NONE) begin
					noneCount++;
				end else begin
					noneCount = 0;
				end 
				if(noneCount <= 3) begin
					$write("\t\t%0d:", i); printObj(localStack[i], reset_strings); $display("");
				end
			end
			$display("\tfp: %0d, frameStack:", fp);
			for(int i = 0; i <= fp; i++) begin
				$display("\t\t%0d: {code: %0d, ra: %0d, prevsp: %0d}", i, 
					frameStack[i].code, frameStack[i].returnAddress, frameStack[i].prevsp); 
			end
			$display("\tGlobals:");
			noneCount = 0;
			for(int i = 0; i < `GLOBALS_SIZE; i++) begin
				if(globals[i].t == NONE) begin
					noneCount++;
				end else begin
					noneCount = 0;
				end 
				if(noneCount <= 3) begin
					$write("\t\t%0d:", i); printObj(globals[i], reset_strings); $display("");
				end
			end
			$display("\tnextList: %0d, listHeapPtr: %0d", nextList, listHeapPtr);
			for(int i = 0; i < nextList; i++) begin
				$display("\t\t%0d: %0d-%0d", i, listStarts[i], listStarts[i] + listSizes[i] - 1);
			end
			$display("\theap:");
			for(int i = 0; i < listHeapPtr; i++) begin
				$write("\t%0d: ", i); printObj(listHeap[i], reset_strings); $display("");
			end
			$display("---------------------------------------------------\n\tir: %0d[%0d], opcode: %0d, arg: %0d;",
					 frameStack[fp].code, ir-reset_codeObjects[frameStack[fp].code].bytecodeStart, opcode, arg);
		end
	endtask
	
	
	
	always @(negedge clk) begin
		if(reset) begin
			$display(  "@@\n@@  %t : System STILL at reset, can't show anything\n@@", $realtime);
		end else begin
			if(halt || cycleCount > 50000000) begin
				$display("@@  %t : System halted\n@@", $realtime);
				$finish;
			end else if(error) begin
				$display(  "@@@ System halted on error");
				$finish;
			end else if(doPrint) begin
				printObj(printData, reset_strings);
			end	
			if(cycleCount < 00)
				printState();
		end
		
	end	
endmodule
