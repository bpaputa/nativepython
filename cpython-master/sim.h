#ifndef SIM_H
#define SIM_H
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "preprocessor.h"
#include "shared.h"

/*
extern Object_t stack[STACK_SIZE];
extern int sp;

extern Object_t localsStack[STACK_SIZE];
extern int localsp;

extern Bytecode_t bytecode[BYTECODE_SIZE];
extern BytecodePtr_t instructionReg;

extern CodeObject_t codeObjects[NUM_FUNCTIONS];
//extern CodeObjectPtr_t methods[NUM_METHODS];

extern FrameObject_t frameStack[CALL_STACK_LIMIT];
extern int framePointer;

extern Object_t consts[CONSTS_SIZE];
extern Object_t globals[GLOBALS_SIZE];
extern char* globalNames[GLOBALS_SIZE];
extern Name_t names[NAMES_SIZE];

extern char strings[STRINGS_SIZE];*/

//extern TypeObject_t types[TYPES_SIZE];

int main();

void run();
void printObj(Object_t obj);
void callFunction(CodeObjectPtr_t ptr);
void returnFromFunction();

int isInteger(int index);

#endif
