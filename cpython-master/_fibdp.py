def __print__(object):
    object

def declList(size):
    return not size

def range(start, limit, step):
    x = (start, step, limit)
    return x

def type(x):
	return x
	
def len(x):
	return x

NoneType = 0
bool = 1
int = 2
str = 3
CodeObject = 4
list = 5
tuple = 6
iter = 7

def print(a):
	if type(a) == list or type(a) == tuple:
		if type(a) == list:
			__print__("[")
		else:
			__print__("(")
		comma = len(a)-1
		for i in a:
			print(i)
			if comma:
				__print__(", ")
				comma = comma - 1
		if type(a) == list:
			__print__("]")
		else:
			__print__(")")
	else:
		__print__(a)

cache = declList(64)
cache[0] = 0
cache[1] = 1
cacheSize = 2

def fib(x):
	global cacheSize
	global cache
	for i in range(cacheSize, x+1, 1):
		cache[i] = cache[i-1] + cache[i-2]
		#cache.append(cache[i-1] + cache[i-2])
		cacheSize += 1
	return cache[x]
	
print(fib(10))
print("\n")
print(fib(20))
print("\n")
