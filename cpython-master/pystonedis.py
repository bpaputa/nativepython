import dis
import pystone

funcs = [pystone.Func1, pystone.Func2, pystone.Func3, pystone.Proc0, pystone.Proc1, pystone.Proc2, pystone.Proc3, pystone.Proc4, pystone.Proc5, pystone.Proc6, pystone.Proc7, pystone.Proc8, pystone.Record.__init__, pystone.Record.copy, pystone.main]

for x in funcs:
	print(x.__code__.co_name)
	for y in x.__code__.co_code:
		print(" " + str(y), end='')
	print('\n')
	dis.dis(x)
