#ifndef PREPROCESSOR_H
#define PREPROCESSOR_H
#include "shared.h"

void init();

//Get Stack Data
int getStack_Obj_data(int i);
int getStack_Obj_type(int i);
int getStack_Obj_numRefs(int i);

//Get LocalsStack Data
int getLocStack_Obj_data(int i);
int getLocStack_Obj_type(int i);
int getLocStack_Obj_numRefs(int i);

//Get Bytecode Data
int getBytecode(int i);

//Get CodeObject Data
int getCO_ArgCount(int i);
int getCO_NLocals(int i);
int getCO_BytecodeStart(int i);
int getCO_ConstStart(int i);
int getCO_NameStart(int i);
int getCO_NumConsts(int i);
int getCO_NumNames(int i);

//Get FrameObject Data
int getFO_CodeObjectPtr(int i);
int getFO_BytecodePtr(int i);
int getFO_prevsp(int i);

//Get consts Data
int getConsts_Obj_data(int i);
int getConsts_Obj_type(int i);
int getConsts_Obj_numRefs(int i);

//Get globals Data
int getGlobals_Obj_data(int i);
int getGlobals_Obj_type(int i);
int getGlobals_Obj_numRefs(int i);

//Get Name Data
int getName_type(int i);
int getName_index(int i);

//Get strings Data
int getString(int i);


//Get listHeap Data
int getListHeap_Obj_data(int i); 
int getListHeap_Obj_type(int i);
int getListHeap_Obj_numRefs(int i);

//Get listStarts Data
int getListStarts(int i);
//Get listSizes Data
int getListSizes(int i);

int getNextList();
int getListHeapPtr();



extern Object_t stack[STACK_SIZE];
extern int sp;

extern Object_t localsStack[STACK_SIZE];
extern int localsp;

extern Bytecode_t bytecode[BYTECODE_SIZE];
extern BytecodePtr_t instructionReg;

extern CodeObject_t codeObjects[NUM_FUNCTIONS];
//extern CodeObjectPtr_t methods[NUM_METHODS];

extern FrameObject_t frameStack[CALL_STACK_LIMIT];
extern int framePointer;

extern Object_t consts[CONSTS_SIZE];
extern Object_t globals[GLOBALS_SIZE];
extern char* globalNames[GLOBALS_SIZE];
extern Name_t names[NAMES_SIZE];

extern char strings[STRINGS_SIZE];
 
#endif
