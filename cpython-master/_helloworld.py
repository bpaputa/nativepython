def __print__(object):
    object

def declList(size):
    return not size

def range(start, limit, step):
    x = (start, step, limit)
    return x

def type(x):
	return x
	
def len(x):
	return x

NoneType = 0
bool = 1
int = 2
str = 3
CodeObject = 4
list = 5
tuple = 6
iter = 7

def print(a):
	if type(a) == list or type(a) == tuple:
		if type(a) == list:
			__print__("[")
		else:
			__print__("(")
		comma = len(a)-1
		for i in a:
			print(i)
			if comma:
				__print__(", ")
				comma = comma - 1
		if type(a) == list:
			__print__("]")
		else:
			__print__(")")
	else:
		__print__(a)


def fib(n):
	x = 0
	if n == 0 or n == 1:
		x = 1
	else:
		x = fib(n-1) + fib(n-2)
	#print("fib(")
	#print(n)
	#print(") = ")
	#print(x)
	#print("\n")
	return x

def fact(n):
	if n:
		#print(n * fact(n-1))
		#print('\n')
		return n * fact(n-1)
	#print(1)
	#print('\n')
	return 1

def main():
	testNum = 10

	print('\n')
	print("Fibonacci Number of ")
	print(testNum)
	print(":\n")
	print(fib(10))
	print("\n")
	print("Factorial of ")
	print(testNum)
	print(":\n")
	print(fact(10))
	print('\n\n')

main()
