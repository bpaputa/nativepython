#include "sim.h"
#include "assert.h"

Object_t stack[STACK_SIZE];
int sp = -1;

Object_t localsStack[STACK_SIZE];
int localsp = 0;

Bytecode_t bytecode[BYTECODE_SIZE];
BytecodePtr_t instructionReg = 0;

CodeObject_t codeObjects[NUM_FUNCTIONS];
//CodeObjectPtr_t methods[NUM_METHODS];

FrameObject_t frameStack[CALL_STACK_LIMIT];
int framePointer = -1;

Object_t consts[CONSTS_SIZE];
Object_t globals[GLOBALS_SIZE];
char* globalNames[GLOBALS_SIZE];
Name_t names[NAMES_SIZE];

char strings[STRINGS_SIZE];

Object_t listHeap[LIST_HEAP_SIZE];
int listStarts[NUM_LISTS];
int listSizes[NUM_LISTS];
int nextList, listHeapPtr;

//extern TypeObject_t types[TYPES_SIZE];


int main(){
	init(/*"pystone.py"*/);
	printf("Output:\n");
	printf("--------------------------------------------------\n");
	run();
	printf("done");
}

void run(){
	callFunction(0);
	while(1){
		int opcode = bytecode[instructionReg];
		int arg = bytecode[instructionReg+1];
		/*
		for(int i = 0; i <= sp; i++){
			printf("    stack[%d]: {t:%d, d:%d}\n", i, stack[i].type, stack[i].data);
		}
		printf("ir: %d[%d], sp: %d, op: %d, arg: %d\n", frameStack[framePointer].code, 
				instructionReg-codeObjects[frameStack[framePointer].code].bytecodeStart, sp, opcode, arg);
		*/
		switch(opcode){
			case POP_TOP:
				sp--;
				break;
			case DUP_TOP:
				stack[sp+1] = stack[sp];
				sp++;
				break;
			case UNARY_NOT:
				if(stack[sp].type == NONE){
					stack[sp] = (Object_t) {1, BOOL, 1};
				} else if(stack[sp].type == STRING){
					stack[sp] = (Object_t) {strings[stack[sp].data] == 0, BOOL, 1}; 
				} else if(stack[sp].type == CODE){
					stack[sp] = (Object_t) {0, BOOL, 1};
				} else {
					stack[sp] = (Object_t) {!stack[sp].data, BOOL, 1};
				}
				break;
			case INPLACE_MULTIPLY:
			case BINARY_MULTIPLY:
				if(isInteger(0) && isInteger(1)){
					stack[sp-1].data = stack[sp].data * stack[sp-1].data;
					stack[sp-1].type = INT;
					sp--;
				} 	else {
					ILLEGAL_OP
				}
				break;/*
			case BINARY_MODULO:
				if(isInteger(0) && isInteger(1)){
					stack[sp-1].data = stack[sp].data % stack[sp-1].data;
					stack[sp-1].type = INT;
					sp--;
				} 	else {
					ILLEGAL_OP
				}
				break;*/
			case INPLACE_ADD:
			case BINARY_ADD:
				if(isInteger(0) && isInteger(1)){
					stack[sp-1].data = stack[sp].data + stack[sp-1].data;
					stack[sp-1].type = INT;
					sp--;
				} 	else {
					ILLEGAL_OP
				}
				break;
			case INPLACE_SUBTRACT:
			case BINARY_SUBTRACT:
				if(isInteger(0) && isInteger(1)){
					stack[sp-1].data = stack[sp-1].data - stack[sp].data;
					stack[sp-1].type = INT;
					sp--;
				} else {
					ILLEGAL_OP
				}
				break;
			case BINARY_SUBSCR:
				if(isInteger(0) && ((stack[sp-1].type == LIST)||(stack[sp-1].type == TUPLE))){
					if(stack[sp].data > listSizes[stack[sp-1].data]){
						ILLEGAL_OP
					}
					stack[sp-1] = listHeap[listStarts[stack[sp-1].data] + stack[sp].data];
					sp--;
				} else {
					ILLEGAL_OP
				}
				break;/*
			case BINARY_TRUE_DIVIDE:
				if(isInteger(0) && isInteger(1)){
					stack[sp-1].data = stack[sp].data / stack[sp-1].data;
					stack[sp-1].type = INT;
					sp--;
				} else {
					ILLEGAL_OP
				}
				break;*/
			case STORE_SUBSCR:
				if(isInteger(0) && (stack[sp-1].type == LIST)){
					if(stack[sp].data > listSizes[stack[sp-1].data]){
						ILLEGAL_OP
					}
					listHeap[listStarts[stack[sp-1].data] + stack[sp].data] = stack[sp-2];
					sp -= 3;
				} else {
					ILLEGAL_OP
				}
				break;
			case GET_ITER:
				if(stack[sp].type == LIST || stack[sp].type == TUPLE){
					listStarts[nextList] = listHeapPtr;
					listHeap[listHeapPtr++] = (Object_t){0, INT};
					listHeap[listHeapPtr++] = (Object_t){1, INT};
					listHeap[listHeapPtr++] = stack[sp];
					listSizes[nextList] = 3;
					stack[sp] = (Object_t){nextList, ITER, 1};
					nextList++;
				} else if(stack[sp].type != ITER){
					ILLEGAL_OP					
				}
					

				break;	
			case BREAK_LOOP:
					//LOOPS TODO

				break;
			case RETURN_VALUE:
				returnFromFunction();
				break;
			case POP_BLOCK:
				//LOOPS TODO
				break;	
			/*case STORE_NAME:
				if(names[codeObjects[frameStack[framePointer].code].nameStart + arg].type == FUNCTION){
					names[codeObjects[frameStack[framePointer].code].nameStart + arg].index = stack[sp--].data;
				} else {
					ILLEGAL_OP
				}
				break;				*/
			case UNPACK_SEQUENCE:
				//(a b) = "ab" TODO
				break;	
			case FOR_ITER:
				if(stack[sp].type != ITER){
					ILLEGAL_OP
				}
				if(listHeap[listStarts[stack[sp].data]+2].type == LIST 
					|| listHeap[listStarts[stack[sp].data]+2].type == TUPLE){
					if(listHeap[listStarts[stack[sp].data]].data >= listSizes[listHeap[listStarts[stack[sp].data]+2].data]){
						sp--;
						instructionReg += arg;
						break;
					} else {
						stack[sp+1] = listHeap[listStarts[listHeap[listStarts[stack[sp].data]+2].data] + listHeap[listStarts[stack[sp].data]].data];
					}
				} else if(listHeap[listStarts[stack[sp].data]+2].type == INT){
					if(((listHeap[listStarts[stack[sp].data]+1].data > 0) && (listHeap[listStarts[stack[sp].data]].data >= listHeap[listStarts[stack[sp].data]+2].data)) ||
						((listHeap[listStarts[stack[sp].data]+1].data < 0) && (listHeap[listStarts[stack[sp].data]].data <= listHeap[listStarts[stack[sp].data]+2].data))){
						sp--;
						instructionReg += arg;
						break;
					} else {
						stack[sp+1] = listHeap[listStarts[stack[sp].data]];
					}					
				} else {
					ILLEGAL_OP
				}
				listHeap[listStarts[stack[sp].data]].data += listHeap[listStarts[stack[sp].data]+1].data;
				sp++;
				break;	
			case STORE_ATTR:
				//OBJECTS TODO
				break;	
			case STORE_NAME:/*
				if(names[codeObjects[frameStack[framePointer].code].nameStart + arg].type != FUNCTION){
					ILLEGAL_OP //TODO Maybe?
				}*/
			case STORE_GLOBAL:
				globals[names[codeObjects[frameStack[framePointer].code].nameStart + arg].index] = stack[sp--];
				break;
			case LOAD_CONST:
				stack[++sp] = consts[codeObjects[frameStack[framePointer].code].constStart + arg];
				break;
			/*case LOAD_NAME:
				if(names[codeObjects[frameStack[framePointer].code].nameStart + arg].type == FUNCTION){
					stack[++sp].type = CODE;
					stack[sp].data = names[codeObjects[frameStack[framePointer].code].nameStart + arg].index;
				} else {
					ILLEGAL_OP
				}
				break;*/
			case BUILD_TUPLE:
				listSizes[nextList] = arg;
				listStarts[nextList] = listHeapPtr;
				for(int i = 0; i < arg; i++){
					listHeap[listHeapPtr+i] = stack[sp-arg+i+1];
				}
				sp -= arg;
				stack[++sp] = (Object_t){nextList, TUPLE, 1};
				listHeapPtr += arg;
				nextList++;
				break;				
			case BUILD_LIST:
				listSizes[nextList] = arg;
				listStarts[nextList] = listHeapPtr;
				for(int i = 0; i < arg; i++){
					listHeap[listHeapPtr+i] = stack[sp-arg+i+1];
				}
				sp -= arg;
				stack[++sp] = (Object_t){nextList, LIST, 1};
				listHeapPtr += arg;
				nextList++;
				
				break;
			case LOAD_ATTR:
				//OBJECTS TODO
				break;
			case COMPARE_OP:
				//WIP TODO
				
				//INTEGERS ONLY
				if(isInteger(0) && isInteger(1)) {
					switch (arg) {
						case LT:
							stack[sp-1].data = stack[sp-1].data < stack[sp].data;
							break;
						case LTE:
							stack[sp-1].data = stack[sp-1].data <= stack[sp].data;
							break;
						case EQ:
							stack[sp-1].data = stack[sp-1].data == stack[sp].data;
							break;
						case NEQ:
							stack[sp-1].data = stack[sp-1].data != stack[sp].data;
							break;
						case GT:
							stack[sp-1].data = stack[sp-1].data > stack[sp].data;
							break;
						case GTE:
							stack[sp-1].data = stack[sp-1].data >= stack[sp].data;
							break;
						case IS:
						case ISNOT:
						case EXCEPTIONMATCH:
						case BAD:
							//TODO
						default:
							ILLEGAL_OP
					}
					sp--;
					stack[sp].type = BOOL;
				}	else if(stack[sp].type != stack[sp-1].type) {
					stack[sp-1] = (Object_t) {0, BOOL, 1};
				}		
				break;
			case JUMP_FORWARD:
				instructionReg += arg;			
				break;
			case JUMP_IF_TRUE_OR_POP:
				//Python 3, TODO
				break;
			case JUMP_ABSOLUTE:
				instructionReg = arg + codeObjects[frameStack[framePointer].code].bytecodeStart-2;
				break;
			case POP_JUMP_IF_FALSE:
				switch(stack[sp].type) {
					case NONE: 
						instructionReg = codeObjects[frameStack[framePointer].code].bytecodeStart + arg - 2;
						break;
					case STRING:
						if(strings[stack[sp].data] == 0) {
							instructionReg = codeObjects[frameStack[framePointer].code].bytecodeStart + arg - 2;
						}
						break;
					case CODE: 
						break;
					default: 
						if(!stack[sp].data){
							instructionReg = codeObjects[frameStack[framePointer].code].bytecodeStart + arg - 2;
						}
				}
				sp--;
				break;
			case POP_JUMP_IF_TRUE:
				switch(stack[sp].type) {
					case NONE: 
						break;
					case STRING:
						if(strings[stack[sp].data] != 0) {
							instructionReg = codeObjects[frameStack[framePointer].code].bytecodeStart + arg - 2;
						}
						break;
					case CODE: 
						instructionReg = codeObjects[frameStack[framePointer].code].bytecodeStart + arg - 2;
						break;
					default: 
						if(stack[sp].data){
							instructionReg = codeObjects[frameStack[framePointer].code].bytecodeStart + arg - 2;
						}
				}
				sp--;
				break;
			case LOAD_NAME:/*
				if(names[codeObjects[frameStack[framePointer].code].nameStart + arg].type != FUNCTION){
					ILLEGAL_OP //TODO Maybe?
				}*/
			case LOAD_GLOBAL:
				stack[++sp] = globals[names[codeObjects[frameStack[framePointer].code].nameStart + arg].index];
				break;
			case SETUP_LOOP:
				//LOOPS TODO
				break;
			case LOAD_FAST:
				/*
				if(instructionReg == 24+codeObjects[7].bytecodeStart){
					printf("error");
				}
				*/
				stack[++sp] = localsStack[localsp + arg];
				break;
			case STORE_FAST:
				localsStack[localsp + arg] = stack[sp--];
				break;
			case CALL_FUNCTION:
				localsp += codeObjects[frameStack[framePointer].code].nlocals;
				for(int i = arg-1; i >= 0; i--){
					localsStack[localsp+i] = stack[sp--];
				}
				if(stack[sp].type != CODE){
					ILLEGAL_OP
				}
				callFunction(stack[sp--].data);
				instructionReg -= 2;
				break;
			case MAKE_FUNCTION:
				if(arg){
					ILLEGAL_OP
				}
				sp--;
				break;				
			case EXTENDED_ARG:
				ILLEGAL_OP
				break;
			case LOAD_METHOD:
				//TODO
				ILLEGAL_OP
				break;
			case CALL_METHOD:
				//TODO
				ILLEGAL_OP
				break;
			case LOAD_FAST_LEN:
				if(localsStack[localsp+arg].type != LIST && localsStack[localsp+arg].type != TUPLE){
					ILLEGAL_OP
				}
				stack[sp+1].data = listSizes[localsStack[localsp+arg].data];
				stack[sp+1].type = INT;
				sp++;
				break;
			case LOAD_FAST_TYPE:				
				stack[sp+1].data = localsStack[localsp+arg].type;
				stack[sp+1].type = INT;
				sp++;
				break;
			case BUILD_RANGE:
				listSizes[nextList] = arg;
				listStarts[nextList] = listHeapPtr;
				for(int i = 0; i < arg; i++){
					listHeap[listHeapPtr+i] = stack[sp-arg+i+1];
				}
				sp -= arg;
				stack[++sp] = (Object_t){nextList, ITER, 1};
				listHeapPtr += arg;
				nextList++;
				break;		
			case DECL_LIST:
				//printf("\nlhptr += %d\n", stack[sp].data);
				listSizes[nextList] = stack[sp].data;
				listStarts[nextList] = listHeapPtr;
				listHeapPtr += stack[sp].data;
				stack[sp] = (Object_t){nextList, LIST, 1};
				nextList++;
				break;
			case PRINT:
				printObj(stack[sp]);
				sp--;
				break;
			default:
				printf("%d\n", opcode);
				ILLEGAL_OP				
		}		
		instructionReg += 2;
	}
}

void printObj(Object_t obj){
	if(obj.type == STRING){
		printf("%s", strings + obj.data);
	} else if(obj.type == CODE){
		printf("<code %d>", obj.data);
	} else if(obj.type == TUPLE || obj.type == LIST){
		if(obj.type == TUPLE){
			printf("(");
		} else {
			printf("[");
		}
		for(int i = 0; i < listSizes[obj.data]; i++){
			printObj(listHeap[listStarts[obj.data] + i]);
			if(i != listSizes[obj.data]-1){
				printf(", ");
			}
		}
		if(obj.type == TUPLE){
			printf(")");
		} else {
			printf("]");
		}		
	} else {
		printf("%d", obj.data);
	}
}

void callFunction(CodeObjectPtr_t ptr){
	frameStack[++framePointer].returnAddress = instructionReg;
	frameStack[framePointer].code = ptr;
	frameStack[framePointer].prevsp = sp;
	instructionReg = codeObjects[ptr].bytecodeStart;
}

void returnFromFunction(){
	if(framePointer == 0){
		exit(0);
	}
	Object_t tempTOS = stack[sp];
	instructionReg = frameStack[framePointer].returnAddress;
	localsp -= codeObjects[frameStack[framePointer-1].code].nlocals;
	sp = frameStack[framePointer].prevsp;
	framePointer--;
	stack[++sp] = tempTOS;
}

int isInteger(int index){
	return (stack[sp - index].type == BOOL || stack[sp - index].type == INT);
}
