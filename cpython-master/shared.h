#ifndef SHARED_H
#define SHARED_H

#include <stdint.h>

#define STACK_SIZE 256
#define BYTECODE_SIZE 4096
#define CONSTS_SIZE 128
#define NAMES_SIZE 64
#define STRINGS_SIZE 512
//#define TYPES_SIZE 64
#define GLOBALS_SIZE 64
#define CALL_STACK_LIMIT 32
#define NUM_FUNCTIONS 64
#define NUM_LISTS 32
#define LIST_HEAP_SIZE 512
//#define NUM_METHODS 64

#define ILLEGAL_OP printf("\nIllegal operation, ir: %d, line: %d\n", instructionReg, __LINE__); exit(1); system("cat compileOutput");

enum opcodes{
	POP_TOP	= 1,
	DUP_TOP	= 4,
	UNARY_NOT	= 12,
	BINARY_MULTIPLY	= 20,
	BINARY_MODULO	= 22,
	BINARY_ADD	= 23,
	BINARY_SUBTRACT	= 24,
	BINARY_SUBSCR	= 25,
	BINARY_TRUE_DIVIDE	= 27,
	INPLACE_ADD 	= 55,
	INPLACE_SUBTRACT= 56,
	INPLACE_MULTIPLY	= 57,
	STORE_SUBSCR	= 60,
	GET_ITER	= 68,
	BREAK_LOOP	= 80,
	RETURN_VALUE	= 83,
	POP_BLOCK	= 87,
	STORE_NAME 	= 90,
	UNPACK_SEQUENCE	= 92,
	FOR_ITER	= 93,
	STORE_ATTR	= 95,
	STORE_GLOBAL	= 97,
	LOAD_CONST	= 100,
	LOAD_NAME 	= 101,
	BUILD_TUPLE	= 102,
	BUILD_LIST	= 103,
	LOAD_ATTR	= 106,
	COMPARE_OP	= 107,
	JUMP_FORWARD	= 110,
	JUMP_IF_TRUE_OR_POP	= 112,
	JUMP_ABSOLUTE	= 113,
	POP_JUMP_IF_FALSE	= 114,
	POP_JUMP_IF_TRUE	= 115,
	LOAD_GLOBAL	= 116,
	SETUP_LOOP	= 120,
	LOAD_FAST	= 124,
	STORE_FAST	= 125,
	CALL_FUNCTION	= 131,
	MAKE_FUNCTION 	= 132,
	EXTENDED_ARG	= 144,
	LOAD_METHOD	= 160,
	CALL_METHOD	= 161,
	LOAD_FAST_LEN = 251,
	LOAD_FAST_TYPE = 252,
	BUILD_RANGE = 253,
	DECL_LIST = 254,
	PRINT = 255
};

enum cmpOps {
	LT,
	LTE,
	EQ,
	NEQ,
	GT,
	GTE,
	IN,
	NOTIN,
	IS,
	ISNOT,
	EXCEPTIONMATCH,
	BAD
};


typedef int CodeObjectPtr_t;
typedef int NamePtr_t;
typedef int BytecodePtr_t;
typedef uint8_t Bytecode_t;
typedef uint8_t Type_t;
typedef int ConstPtr_t;

struct CodeObject {
	int argcount;
	int nlocals;
	//int stacksize;
	//uint8_t flags;
	BytecodePtr_t bytecodeStart;
	ConstPtr_t constStart;
	NamePtr_t nameStart;
	int numConsts;
	int numNames;
};

typedef struct CodeObject CodeObject_t;

struct FrameObject {
	CodeObjectPtr_t code;
	BytecodePtr_t returnAddress;
	int prevsp;
};

typedef struct FrameObject FrameObject_t;

enum TypeLabel {
	NONE, BOOL, INT, STRING, CODE, LIST, TUPLE, ITER
};

struct TypeObject {
	int numMethods;
	int methodsStart;
};

typedef struct TypeObject TypeObject_t;

struct Object {
	uint32_t data;
	Type_t type;
	uint8_t numRefs;
};

typedef struct Object Object_t;


#define FUNCTION 0
#define METHOD 1
#define GLOBAL 2
#define LOCAL 3
struct Name {
	uint8_t type;
	uint8_t index;
};

typedef struct Name Name_t;
#endif
