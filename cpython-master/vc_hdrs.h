#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <stdio.h>
#include <dlfcn.h>
#include "svdpi.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _VC_TYPES_
#define _VC_TYPES_
/* common definitions shared with DirectC.h */

typedef unsigned int U;
typedef unsigned char UB;
typedef unsigned char scalar;
typedef struct { U c; U d;} vec32;

#define scalar_0 0
#define scalar_1 1
#define scalar_z 2
#define scalar_x 3

extern long long int ConvUP2LLI(U* a);
extern void ConvLLI2UP(long long int a1, U* a2);
extern long long int GetLLIresult();
extern void StoreLLIresult(const unsigned int* data);
typedef struct VeriC_Descriptor *vc_handle;

#ifndef SV_3_COMPATIBILITY
#define SV_STRING const char*
#else
#define SV_STRING char*
#endif

#endif /* _VC_TYPES_ */

int getStack_Obj_data(int i);
int getStack_Obj_type(int i);
int getStack_Obj_numRefs(int i);
int getLocStack_Obj_data(int i);
int getLocStack_Obj_type(int i);
int getLocStack_Obj_numRefs(int i);
int getBytecode(int i);
int getCO_ArgCount(int i);
int getCO_NLocals(int i);
int getCO_BytecodeStart(int i);
int getCO_ConstStart(int i);
int getCO_NameStart(int i);
int getCO_NumConsts(int i);
int getCO_NumNames(int i);
int getFO_CodeObjectPtr(int i);
int getFO_BytecodePtr(int i);
int getFO_prevsp(int i);
int getConsts_Obj_data(int i);
int getConsts_Obj_type(int i);
int getConsts_Obj_numRefs(int i);
int getGlobals_Obj_data(int i);
int getGlobals_Obj_type(int i);
int getGlobals_Obj_numRefs(int i);
int getName_type(int i);
int getName_index(int i);
int getString(int i);
int getListHeap_Obj_data(int i);
int getListHeap_Obj_type(int i);
int getListHeap_Obj_numRefs(int i);
int getListStarts(int i);
int getListSizes(int i);
int getNextList();
int getListHeapPtr();
void init();

#ifdef __cplusplus
}
#endif

