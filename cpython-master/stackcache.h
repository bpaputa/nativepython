#ifndef STACKCACHE_H
#define STACKCACHE_H
#include "datacache.h"

const int high_mark = 52;
const int low_mark = 8;

PyObject** stackbase = 0;

PyObject** sc_optop = 0;
PyObject** sc_bottom = 0;

int accessStack(PyObject** accessTop, int numAccesses, long long int cycle){
	if(stackbase){
		stackbase = min(stackbase, accessTop - numAccesses);
	} else {
		stackbase = accessTop - numAccesses;
	}
	
}

#endif
