#include "preprocessor.h"
#include "sim.h"

Object_t stack[STACK_SIZE];
int sp;

Object_t localsStack[STACK_SIZE];
int localsp;

Bytecode_t bytecode[BYTECODE_SIZE];
BytecodePtr_t instructionReg;

CodeObject_t codeObjects[NUM_FUNCTIONS];
//CodeObjectPtr_t methods[NUM_METHODS];

FrameObject_t frameStack[CALL_STACK_LIMIT];
int framePointer;

Object_t consts[CONSTS_SIZE];
Object_t globals[GLOBALS_SIZE];
char* globalNames[GLOBALS_SIZE];
Name_t names[NAMES_SIZE];

char strings[STRINGS_SIZE];

Object_t listHeap[LIST_HEAP_SIZE];
int listStarts[NUM_LISTS];
int listSizes[NUM_LISTS];
int nextList = 0, listHeapPtr = 0;

void replaceNameTypes();
int readCodeObject(FILE* file);


//Getter Functions

//Get Stack Data
int getStack_Obj_data(int i) {
	return stack[i].data;
}
int getStack_Obj_type(int i) {
	return stack[i].type;
}
int getStack_Obj_numRefs(int i) {
	return stack[i].numRefs;
}

//Get LocalsStack Data
int getLocStack_Obj_data(int i) {
	return localsStack[i].data;
}
int getLocStack_Obj_type(int i) {
	return localsStack[i].type;
}
int getLocStack_Obj_numRefs(int i) {
	return localsStack[i].type;
}

//Get Bytecode
int getBytecode(int i){
	return bytecode[i];
}

//Get CodeObject Data
int getCO_ArgCount(int i){
	return codeObjects[i].argcount;
}
int getCO_NLocals(int i){
	return codeObjects[i].nlocals;
}
int getCO_BytecodeStart(int i) {
	return codeObjects[i].bytecodeStart;
}
int getCO_ConstStart(int i) {
	return codeObjects[i].constStart;
}
int getCO_NameStart(int i) {
	return codeObjects[i].nameStart;
}
int getCO_NumConsts(int i) {
	return codeObjects[i].numConsts;
}
int getCO_NumNames(int i) {
	return codeObjects[i].numNames;
}

//Get FrameObject Data
int getFO_CodeObjectPtr(int i) {
	return frameStack[i].code;
}
int getFO_BytecodePtr(int i) {
	return frameStack[i].returnAddress;
}
int getFO_prevsp(int i) {
	return frameStack[i].prevsp;
}

//Get consts Data
int getConsts_Obj_data(int i) {
	return consts[i].data;
}
int getConsts_Obj_type(int i) {
	return consts[i].type;
}
int getConsts_Obj_numRefs(int i) {
	return consts[i].numRefs;
}

//Get globals Data
int getGlobals_Obj_data(int i) {
	return globals[i].data;
}
int getGlobals_Obj_type(int i) {
	return globals[i].type;
}
int getGlobals_Obj_numRefs(int i) {
	return globals[i].numRefs;
}

//Get Name Data
int getName_type(int i) {
	return names[i].type;
}
int getName_index(int i) {
	return names[i].index;
}

//Get strings Data
int getString(int i) {
	return strings[i];
}

//Get listHeap Data
int getListHeap_Obj_data(int i) {
	return listHeap[i].data;
}
int getListHeap_Obj_type(int i) {
	return listHeap[i].type;
}
int getListHeap_Obj_numRefs(int i) {
	return listHeap[i].numRefs;
}

//Get listStarts Data
int getListStarts(int i) {
	return listStarts[i];
}
//Get listSizes Data
int getListSizes(int i) {
	return listSizes[i];
}

int getNextList() {
	return nextList;
}
int getListHeapPtr() {
	return listHeapPtr;
}



void init(){
	char* code = malloc(100);
	strcpy(code, "./python compile.py ");
	strcat(code, "helloworld.py");
	strcat(code, " > compileOutput");
	system(code);
	free(code); 
	
	FILE* file = fopen("compileOutput", "r");
	
	printf("\nPreprocessor Started\n");
	readCodeObject(file);
	replaceNameTypes();
	fclose(file);
	printf("Preprocessor Completed\n\n");
	
}


int nextGlobal = 0;
int nextName = 0;
int nextConst = 0;
int nextBytecode = 0;
int nextCodeObject = 0;
int nextString = 0;

char* funcNames[NUM_FUNCTIONS];
char* nameNames[NAMES_SIZE];

Object_t readConst(FILE* file){	
	char type[64];
	fscanf(file, "%s", type);
	//printf("%s\n", type);
	if(!strcmp(type, "NoneType")){
		char junk[8];
		fscanf(file, "%s", junk);
		return (Object_t) {0, NONE, 1};
	} else if(!strcmp(type, "bool")){
		char value[16];
		fscanf(file, "%s", value);
		if(!strcmp(value, "True")){
			return (Object_t) {1, BOOL, 1};
		} else if(!strcmp(value, "False")){
			return (Object_t) {0, BOOL, 1};
		}
	} else if(!strcmp(type, "int")){
		int value;
		char buf[100];
		fscanf(file, "%s", buf);
		value = atoi(buf);
		//printf("%d\n", value);
		return (Object_t) {value, INT, 1};
	} else if(!strcmp(type, "long")){
		ILLEGAL_OP
	} else if(!strcmp(type, "float")){
		ILLEGAL_OP
	} else if(!strcmp(type, "str")) {
		int thisString = nextString;
		char value;
		int first = 1;
		do {
			value = fgetc(file);
			if(first) {
				first = 0;
				if(value != '\n'){
					strings[nextString] = value;	
					nextString++;
				}
			} else {
				strings[nextString] = value;	
				nextString++;
			}			
		} while(value != '\0');
		return (Object_t) {thisString, STRING, 1};
		
	} else if(!strcmp(type, "code")) {
		return (Object_t) {readCodeObject(file), CODE, 1};
	} else if(!strcmp(type, "tuple")) {
		int size;
		char buf[100];
		fscanf(file, "%s", buf);
		size = atoi(buf);
		int thisTuple = nextList++;
		listSizes[thisTuple] = size;
		listStarts[thisTuple] = listHeapPtr;
		listHeapPtr += size;
		for(int i = 0; i < size; i++){
			listHeap[listStarts[thisTuple] + i] = readConst(file);
		}
		return (Object_t) {thisTuple, TUPLE, 1};
	} else {
		ILLEGAL_OP
	}
}

int readCodeObject(FILE* file){
	int curCodeObjectPtr = nextCodeObject++;	
	char name[64];
	fscanf(file, "%63s %d", name, &codeObjects[curCodeObjectPtr].argcount);//, 
									//&codeObjects[curCodeObjectPtr].stacksize);
	funcNames[curCodeObjectPtr] = malloc(strlen(name)+1);
	strcpy(funcNames[curCodeObjectPtr], name);
	
	printf("\tReading code object: %s", name);
	codeObjects[curCodeObjectPtr].nameStart = nextName;
	
	codeObjects[curCodeObjectPtr].bytecodeStart = nextBytecode;
	
	int codelen;
	fscanf(file, "%d", &codelen);
	fgetc(file);
	int specialPrintFunc = !strcmp(name, "__print__"), 
		specialDeclListFunc = !strcmp(name, "declList"), 
		specialRangeFunc = !strcmp(name, "range"),
		specialTypeFunc = !strcmp(name, "type"),
		specialLenFunc = !strcmp(name, "len");
	for(int i = 0; i < codelen; i++){
		char buf[3] = {fgetc(file), fgetc(file), 0};
		//printf(" %s", buf);
		fgetc(file);
		char* end;
		int byte = strtol(buf, &end, 16);
		if(specialPrintFunc && byte == POP_TOP){
			bytecode[nextBytecode++] = PRINT;
		} else if(specialDeclListFunc && byte == UNARY_NOT){
			bytecode[nextBytecode++] = DECL_LIST;
		} else if(specialRangeFunc && byte == BUILD_TUPLE){
			bytecode[nextBytecode++] = BUILD_RANGE;
		} else if(specialTypeFunc && byte == LOAD_FAST){
			bytecode[nextBytecode++] = LOAD_FAST_TYPE;
		}else if(specialLenFunc && byte == LOAD_FAST){
			bytecode[nextBytecode++] = LOAD_FAST_LEN;
		} else {
			bytecode[nextBytecode++] = byte;
		}
//		fscanf(file, "%2hhx", bytecode + nextBytecode++);
		//printf(" %d", bytecode[nextBytecode-1]);
	} 
	printf("\n");
	
	//Globals
	fscanf(file, "%d", &codeObjects[curCodeObjectPtr].numNames);
	char lglobalNames[128][64];
	for(int i = 0; i < codeObjects[curCodeObjectPtr].numNames; i++){
		fscanf(file, "%s", &lglobalNames[i]);
		int found = -1;
		for(int j = 0; j < nextGlobal; j++){
			if(!strcmp(lglobalNames[i], globalNames[j])){
				found = j;
				break;
			}
		}
		nameNames[nextName] = malloc(strlen(lglobalNames[i]) + 1);
		strcpy(nameNames[nextName], lglobalNames[i]);
		if(found == -1){
			globalNames[nextGlobal] = malloc(strlen(lglobalNames[i])+1);
			strcpy(globalNames[nextGlobal], lglobalNames[i]);
			globals[nextGlobal] = (Object_t) {0, NONE, 1};
			names[nextName] = (Name_t) {GLOBAL, nextGlobal};
			nextGlobal++;
			nextName++;
		} else {
			globals[found].numRefs++;
			names[nextName] = (Name_t) {GLOBAL, found};
			nextName++; 
		}
	}
	
	
	//Locals
	fscanf(file, "%d", &codeObjects[curCodeObjectPtr].nlocals);
	char llocals[128][64];
	for(int i = 0; i < codeObjects[curCodeObjectPtr].nlocals; i++){
		fscanf(file, "%s", &llocals[i]);
	}
	
	//Constants

	fscanf(file, "%d", &codeObjects[curCodeObjectPtr].numConsts);
	int ourNextConst = nextConst;
	codeObjects[curCodeObjectPtr].constStart = ourNextConst;
	nextConst += codeObjects[curCodeObjectPtr].numConsts;
	for(int i = 0; i < codeObjects[curCodeObjectPtr].numConsts; i++){
		consts[ourNextConst++] = readConst(file);
	}
		
	return curCodeObjectPtr;
}

void replaceNameTypes(){
	for(int i = 0; i < nextName; i++){
		for(int j = 0; j < nextCodeObject; j++){
			if(!strcmp(nameNames[i], funcNames[j])){
				names[i].type = FUNCTION;
				//names[i].index = j;
				break;
			}
		}
	}
}

