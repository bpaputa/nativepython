cache = declList(64)
cache[0] = 0
cache[1] = 1
cacheSize = 2

def fib(x):
	global cacheSize
	global cache
	for i in range(cacheSize, x+1, 1):
		cache[i] = cache[i-1] + cache[i-2]
		#cache.append(cache[i-1] + cache[i-2])
		cacheSize += 1
	return cache[x]
	
print(fib(10))
print("\n")
print(fib(20))
print("\n")
