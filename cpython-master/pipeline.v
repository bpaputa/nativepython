
`include "sys_defs.vh"
`timescale 1ns/100ps

module pipeline(
	input clk,
	input reset,
	
	input Bytecode[`BYTECODE_SIZE-1:0] 			reset_bytecode,
	input CodeObject[`CODE_OBJECTS_SIZE-1:0]	reset_codeObjects,
	input Object[`CONSTS_SIZE-1:0] 				reset_consts,
	input Object[`GLOBALS_SIZE-1:0] 			reset_globals,
	input Name[`NAMES_SIZE-1:0] 				reset_names,	
	input Char[`STRINGS_SIZE-1:0] 				reset_strings,
	input Object[`LIST_HEAP_SIZE-1:0] 			reset_listHeap,
	input ListHeapPtr[`NUM_LISTS-1:0] 			reset_listStarts,
	input ListSize[`NUM_LISTS-1:0] 				reset_listSizes,
	input logic[`LIST_PTR_SIZE-1:0] 			reset_nextList,
	input ListHeapPtr 							reset_listHeapPtr,
	
	output logic error,
	output logic halt,
	
	output logic doPrint,
	output Object printData,
	
	output Object[`STACK_SIZE-1:0] 	stack,
	output logic[`SP_SIZE-1:0]		sp, 
	output Object[`STACK_SIZE-1:0] 	localStack,
	output logic[`SP_SIZE-1:0]		localsp,
	
	output logic[`BYTECODE_PTR_SIZE-1:0] ir, 
	output logic[7:0] opcode,
	output logic[7:0] arg,
	
	
	output FrameObject[`CALL_STACK_LIMIT-1:0]	frameStack,
	output logic [`CALL_SP_SIZE-1:0]			fp,
	
	output Object[`LIST_HEAP_SIZE-1:0] 	listHeap,
	output ListHeapPtr[`NUM_LISTS-1:0] 	listStarts,
	output ListSize[`NUM_LISTS-1:0] 	listSizes, 
	output logic[`LIST_PTR_SIZE-1:0] 	nextList,	
	output ListHeapPtr 					listHeapPtr,
	
	output Object[`GLOBALS_SIZE-1:0] 	globals
	
	);
	
	Object[`STACK_SIZE-1:0] nextStack, nextLocalStack;
	logic[`SP_SIZE-1:0]	nextsp, nextLocalsp, sp_p1, sp_m1, sp_m2, sp_m3 ;
	

	Bytecode[`BYTECODE_SIZE-1:0] 	bytecode;
	logic[`BYTECODE_PTR_SIZE-1:0] 	nextir;
	
	CodeObject[`CODE_OBJECTS_SIZE-1:0]	codeObjects;
	
	FrameObject[`CALL_STACK_LIMIT-1:0]		nextFrameStack;
	logic [`CALL_SP_SIZE-1:0]				nextfp, fp_p1, fp_m1;
	
	logic[`BYTECODE_PTR_SIZE-1:0] ir_p1, ir_m1;
	
	Object[`CONSTS_SIZE-1:0] consts;
	Object[`GLOBALS_SIZE-1:0] nextglobals;
	Name[`NAMES_SIZE-1:0] names, nextnames;
	
	Char[`STRINGS_SIZE-1:0] strings;


	Object[`LIST_HEAP_SIZE-1:0] nextListHeap;
	ListHeapPtr[`NUM_LISTS-1:0] nextListStarts;
	ListSize[`NUM_LISTS-1:0] 	nextListSizes;
	logic[`LIST_PTR_SIZE-1:0] 	nextNextList;
	ListHeapPtr 				nextListHeapPtr;
	
	logic nextError, nextHalt;
	
	logic nextDoPrint;
	Object nextPrintData;
	
	function signed_lt;
		input [31:0] a, b;
		if (a[31] == b[31]) begin 
			signed_lt = (a < b); // signs match: signed compare same as unsigned
		end else begin
			signed_lt = a[31];   // signs differ: a is smaller if neg, larger if pos
		end
	endfunction
	
	function signed_gt;
		input [31:0] a, b;
		if (a[31] == b[31]) begin 
			signed_gt = (a > b); // signs match: signed compare same as unsigned
		end else begin
			signed_gt = b[31];   // signs differ: b is smaller if neg, larger if pos
		end
	endfunction
	
	function signed_lte;
		input [31:0] a, b;
		if (a[31] == b[31]) begin 
			signed_lte = (a <= b); // signs match: signed compare same as unsigned
		end else begin
			signed_lte = a[31];   // signs differ: a is smaller if neg, larger if pos
		end
	endfunction
	
	function signed_gte;
		input [31:0] a, b;
		if (a[31] == b[31]) begin 
			signed_gte = (a >= b); // signs match: signed compare same as unsigned
		end else begin
			signed_gte = b[31];   // signs differ: b is smaller if neg, larger if pos
		end
	endfunction
	
	
	
	assign opcode = bytecode[ir];
	assign arg = bytecode[ir_p1];
	
	Object TOS, TOS1;
	assign TOS = stack[sp];
	assign TOS1 = stack[sp_m1];	

	assign sp_p1 = sp + 1;
	assign sp_m1 = sp - 1;
	assign sp_m2 = sp - 2;
	assign sp_m3 = sp - 3;
	assign ir_p1 = ir + 1;
	assign ir_m1 = ir - 1;
	assign fp_p1 = fp + 1;
	assign fp_m1 = fp - 1;
	
	always_comb begin
		nextStack 		= stack;     
		nextLocalStack 	= localStack;
		nextsp 			= sp;        
		nextLocalsp 	= localsp;   
		nextir 			= ir;        
		nextFrameStack 	= frameStack;
		nextfp 			= fp;        
		nextglobals 	= globals;   
		nextnames 		= names;
		nextListHeap 	= listHeap;
		nextListStarts	= listStarts;
		nextListSizes	= listSizes;
		nextNextList	= nextList;
		nextListHeapPtr	= listHeapPtr;
		nextError 		= 0;
		nextHalt		= 0;
		nextDoPrint 		= 0;
		nextPrintData.t 	= NONE;
		nextPrintData.data	= 0;
		case(opcode) 
			POP_TOP: begin
				nextsp = sp_m1;
			end
			DUP_TOP: begin
				nextsp = sp_p1;
				nextStack[sp] = TOS1;
			end
			UNARY_NOT: begin
				nextStack[sp].t = BOOL;
				case(TOS.t) 
					NONE: begin
						nextStack[sp].data = 01;
					end
					STRING: begin
						nextStack[sp].data = strings[TOS.data] == 0;
					end
					CODE: begin
						nextStack[sp].data = 0;
					end
					default: begin
						nextStack[sp].data = !TOS.data;
					end
				endcase
			end
			BINARY_MULTIPLY: begin
				nextStack[sp_m1].data = stack[sp].data * stack[sp_m1].data;
				nextStack[sp_m1].t = INT;
				nextsp = sp_m1;
			end 
			BINARY_MODULO: begin
				`ERROR(("NOT SUPPORTED"));
			end
			BINARY_ADD: begin
				nextStack[sp_m1].data = stack[sp].data + stack[sp_m1].data;
				nextStack[sp_m1].t = INT;
				nextsp = sp_m1;
			end
			BINARY_SUBTRACT: begin
				nextStack[sp_m1].data = stack[sp_m1].data - stack[sp].data;
				nextStack[sp_m1].t = INT;
				nextsp = sp_m1;
			end
			INPLACE_MULTIPLY: begin
				nextStack[sp_m1].data = stack[sp].data * stack[sp_m1].data;
				nextStack[sp_m1].t = INT;
				nextsp = sp_m1;
			end 
			INPLACE_MODULO: begin
				`ERROR(("NOT SUPPORTED"));
			end
			INPLACE_ADD: begin
				nextStack[sp_m1].data = stack[sp].data + stack[sp_m1].data;
				nextStack[sp_m1].t = INT;
				nextsp = sp_m1;
			end
			INPLACE_SUBTRACT: begin
				nextStack[sp_m1].data = stack[sp_m1].data - stack[sp].data;
				nextStack[sp_m1].t = INT;
				nextsp = sp_m1;
			end
			BINARY_SUBSCR: begin
				if(TOS.t == INT && ((TOS1.t == LIST)||(TOS1.t == TUPLE))) begin
					if(TOS.data > listSizes[TOS1.data]) begin
						`ERROR(("NOT SUPPORTED"));
					end
					nextStack[sp_m1] = listHeap[listStarts[TOS1.data] + TOS.data];
					nextsp = sp_m1;
				end else begin
					`ERROR(("NOT SUPPORTED"));
				end
			end
			BINARY_TRUE_DIVIDE: begin
				`ERROR(("NOT SUPPORTED"));
			end
			STORE_SUBSCR: begin
				if(TOS.t == INT && (TOS1.t == LIST)) begin
					if(TOS.data > listSizes[TOS1.data]) begin
						`ERROR(("NOT SUPPORTED"));
					end
					nextListHeap[listStarts[TOS1.data] + TOS.data] = stack[sp_m2];
					nextsp = sp_m3;
				end else begin
					`ERROR(("NOT SUPPORTED"));
				end
			end
			GET_ITER: begin
				if(stack[sp].t == LIST || stack[sp].t == TUPLE) begin
					nextListStarts[nextList] = listHeapPtr;
					nextListHeapPtr = listHeapPtr + 3; 
					nextListHeap[listHeapPtr] = {32'b0, INT};
					nextListHeap[listHeapPtr+1] = {32'b01, INT};
					nextListHeap[listHeapPtr+2] = stack[sp];
					nextListSizes[nextList] = 3;
					nextStack[sp] = {nextList, ITER};
					nextNextList = nextList + 1;
				end else if(stack[sp].t != ITER) begin
					`ERROR(("NOT SUPPORTED"));				
				end
			end
			BREAK_LOOP: begin
				`ERROR(("NOT SUPPORTED"));
			end
			RETURN_VALUE: begin
				if(fp == 0) begin
					nextHalt = 1;
				end else begin
					nextir = frameStack[fp].returnAddress;
					nextLocalsp = localsp - codeObjects[frameStack[fp_m1].code].nlocals;
					nextsp = frameStack[fp].prevsp;
					nextfp = fp_m1;
					nextsp = nextsp + 1; //TODO?
					nextStack[nextsp] = TOS;
				end
			end
			POP_BLOCK: begin
				//`ERROR(("NOT SUPPORTED"));
			end
			STORE_NAME: begin
				nextglobals[names[codeObjects[frameStack[fp].code].nameStart + arg].index] = stack[sp];
				nextsp = sp_m1;
			end
			UNPACK_SEQUENCE: begin
				`ERROR(("NOT SUPPORTED"));
			end
			FOR_ITER: begin
				if(stack[sp].t != ITER) begin
					`ERROR(("NOT SUPPORTED"));
				end
				if(listHeap[listStarts[stack[sp].data]+2].t == LIST 
					|| listHeap[listStarts[stack[sp].data]+2].t == TUPLE) begin
					if(listHeap[listStarts[stack[sp].data]].data >= listSizes[listHeap[listStarts[stack[sp].data]+2].data]) begin
						nextsp = sp_m1;
						nextir = ir + arg;
					end else begin
						nextStack[sp_p1] = listHeap[listStarts[listHeap[listStarts[stack[sp].data]+2].data] + listHeap[listStarts[stack[sp].data]].data];
					end
				end else if(listHeap[listStarts[stack[sp].data]+2].t == INT) begin
					if((!listHeap[listStarts[stack[sp].data]+1].data[31] && signed_gte(listHeap[listStarts[stack[sp].data]].data, listHeap[listStarts[stack[sp].data]+2].data))||
						(listHeap[listStarts[stack[sp].data]+1].data[31] && signed_lte(listHeap[listStarts[stack[sp].data]].data, listHeap[listStarts[stack[sp].data]+2].data))) begin
						nextsp = sp_m1;
						nextir = ir + arg;
					end else begin
						nextStack[sp_p1] = listHeap[listStarts[stack[sp].data]];
					end					
				end else begin
					`ERROR(("NOT SUPPORTED"));
				end
				
				if(nextir == ir) begin
					nextListHeap[listStarts[stack[sp].data]].data += listHeap[listStarts[stack[sp].data]+1].data;
					nextsp = sp_p1;
				end
						
			end
			STORE_ATTR: begin
				`ERROR(("NOT SUPPORTED"));
			end
			STORE_GLOBAL: begin
				nextglobals[names[codeObjects[frameStack[fp].code].nameStart + arg].index] = stack[sp];
				nextsp = sp_m1;
			end
			LOAD_CONST: begin
				nextsp = sp_p1;				
				nextStack[sp_p1] = consts[codeObjects[frameStack[fp].code].constStart + arg];
			end
			LOAD_NAME: begin
				nextsp = sp_p1;
				nextStack[nextsp] = globals[names[codeObjects[frameStack[fp].code].nameStart + arg].index];
			end
			BUILD_TUPLE: begin
				nextListSizes[nextList] = arg;
				nextListStarts[nextList] = listHeapPtr;
				if(arg > `MAX_ARGS) begin
					`ERROR(("NOT SUPPORTED"));					
				end else begin
					for(int unsigned i = 0; i < arg && i < `MAX_ARGS; i++) begin
						nextListHeap[listHeapPtr+i] = stack[sp-arg+i+1];
					end
					nextsp = sp - arg + 1;
					nextStack[nextsp] = {nextList, TUPLE};
					nextListHeapPtr = listHeapPtr + arg;
					nextNextList = nextList + 1;		
				end	
			end
			BUILD_LIST: begin
				nextListSizes[nextList] = arg;
				nextListStarts[nextList] = listHeapPtr;
				if(arg > `MAX_ARGS) begin
					`ERROR(("NOT SUPPORTED"));					
				end else begin
					for(int unsigned i = 0; i < arg && i < `MAX_ARGS; i++) begin
						nextListHeap[listHeapPtr+i] = stack[sp-arg+i+1];
					end
					nextsp = sp - arg + 1;
					nextStack[nextsp] = {nextList, LIST};
					nextListHeapPtr = listHeapPtr + arg;
					nextNextList = nextList + 1;		
				end	
			end
			LOAD_ATTR: begin
				`ERROR(("NOT SUPPORTED"));
			end
			COMPARE_OP: begin
				if( (TOS.t == BOOL || TOS.t == INT) && (TOS1.t == BOOL || TOS1.t == INT) ) begin
					nextsp = sp_m1;
					case(arg)
						LT: begin
							nextStack[nextsp].data = signed_lt(stack[nextsp].data, stack[sp].data);
						end
						LTE: begin
							nextStack[nextsp].data = signed_lte(stack[nextsp].data, stack[sp].data);
						end
						EQ: begin
							nextStack[nextsp].data = stack[nextsp].data == stack[sp].data;
						end
						NEQ: begin
							nextStack[nextsp].data = stack[nextsp].data != stack[sp].data;
						end
						GT: begin
							nextStack[nextsp].data = signed_gt(stack[nextsp].data, stack[sp].data);
						end
						GTE: begin
							nextStack[nextsp].data = signed_gte(stack[nextsp].data, stack[sp].data);
						end
						IS: begin
							nextStack[nextsp].data = (stack[sp].data == stack[nextsp].data) && 
													(stack[sp].t == stack[nextsp].t);
						end
						ISNOT: begin
							nextStack[nextsp].data = (stack[sp].data != stack[nextsp].data) && 
													(stack[sp].t != stack[nextsp].t);
						end
						EXCEPTIONMATCH: begin
							`ERROR(("NOT SUPPORTED"));
						end
						BAD: begin
							`ERROR(("NOT SUPPORTED"));
						end
						default: begin
							`ERROR(("NOT SUPPORTED"));
						end
					endcase
				end	else if(TOS.t != TOS1.t) begin //TODO
					nextsp = sp_m1;
					nextStack[nextsp].data = 0;
					nextStack[nextsp].t = BOOL;
				end	
			end
			JUMP_FORWARD: begin
				nextir = ir + arg;
			end
			JUMP_IF_TRUE_OR_POP: begin
				`ERROR(("NOT SUPPORTED"));
			end
			JUMP_ABSOLUTE: begin		
				nextir = arg + codeObjects[frameStack[fp].code].bytecodeStart;
			end
			POP_JUMP_IF_FALSE: begin
				case(TOS.t) 
					NONE: begin
						nextir = codeObjects[frameStack[fp].code].bytecodeStart + arg - 2;
					end
					STRING: begin
						if(strings[TOS.data] == 0) begin
							nextir = codeObjects[frameStack[fp].code].bytecodeStart + arg - 2;
						end
					end
					CODE: begin
						
					end
					default: begin
						if(!TOS.data) begin
							nextir = codeObjects[frameStack[fp].code].bytecodeStart + arg - 2;
						end
					end
				endcase
				nextsp = sp_m1;
			end
			POP_JUMP_IF_TRUE: begin
				case(TOS.t) 
					NONE: begin
					end
					STRING: begin
						if(strings[TOS.data] != 0) begin
							nextir = codeObjects[frameStack[fp].code].bytecodeStart + arg - 2;
						end
					end
					CODE: begin
						nextir = codeObjects[frameStack[fp].code].bytecodeStart + arg - 2;				
					end
					default: begin
						if(TOS.data) begin
							nextir = codeObjects[frameStack[fp].code].bytecodeStart + arg - 2;
						end
					end
				endcase
				nextsp = sp_m1;
			end
			LOAD_GLOBAL: begin
				nextStack[sp_p1] = globals[names[codeObjects[frameStack[fp].code].nameStart + arg].index];
				nextsp = sp_p1;
			end
			SETUP_LOOP: begin
				//`ERROR(("NOT SUPPORTED"));
			end
			LOAD_FAST: begin
				nextStack[sp_p1] = localStack[localsp+arg];
				nextsp = sp_p1;
			end
			STORE_FAST: begin
				nextLocalStack[localsp + arg] = stack[sp];
				nextsp = sp_m1;
			end
			CALL_FUNCTION: begin
				nextLocalsp = localsp + codeObjects[frameStack[fp].code].nlocals;
				if(arg < `MAX_ARGS && arg != 0) begin
					for(int unsigned i = 0; i < `MAX_ARGS; i++) begin
						if( i <= arg-1) begin
							nextLocalStack[nextLocalsp+i] = stack[nextsp-arg+i+1];
						end
					end
					nextsp = nextsp - arg;
				end
				if(stack[nextsp].t != CODE) begin
					`ERROR(("NOT SUPPORTED"));
				end 

				nextfp = fp_p1;
				nextFrameStack[fp_p1].returnAddress = ir;
				nextFrameStack[fp_p1].code = stack[nextsp].data;
				nextir = codeObjects[stack[nextsp].data].bytecodeStart;
				nextsp = nextsp - 1;
				nextFrameStack[fp_p1].prevsp = nextsp;
			end
			MAKE_FUNCTION: begin
				nextError = arg && 1;
				nextsp = sp_m1;
			end
			EXTENDED_ARG: begin
				`ERROR(("NOT SUPPORTED"));
			end
			LOAD_METHOD: begin
				`ERROR(("NOT SUPPORTED"));
			end
			CALL_METHOD: begin
				`ERROR(("NOT SUPPORTED"));
			end
			LOAD_FAST_LEN: begin
				if(localStack[localsp+arg].t != LIST && localStack[localsp+arg].t != TUPLE) begin
					`ERROR(("NOT SUPPORTED"));
				end
				nextStack[sp_p1].data = listSizes[localStack[localsp+arg].data];
				nextStack[sp_p1].t = INT;
				nextsp = sp_p1;
			end		
			LOAD_FAST_TYPE: begin
				nextStack[sp_p1].data = localStack[localsp+arg].t;
				nextStack[sp_p1].t = INT;
				nextsp = sp_p1;
			end			
			BUILD_RANGE: begin
				nextListSizes[nextList] = 3;
				nextListStarts[nextList] = listHeapPtr;
				for(int unsigned i = 0; i < 3; i++) begin
					nextListHeap[listHeapPtr+i] = stack[sp-arg+i+1];
				end	
				nextsp = sp_m2;
				nextStack[nextsp] = {nextList, ITER};
				nextListHeapPtr = listHeapPtr + 3;
				nextNextList = nextList + 1;
			end

			DECL_LIST: begin
				nextListSizes[nextList] = stack[sp].data;
				nextListStarts[nextList] = listHeapPtr;
				nextStack[sp] = {nextList, LIST};
				nextListHeapPtr = listHeapPtr + stack[sp].data;
				nextNextList = nextList + 1;
			end 
			
			PRINT: begin
				nextDoPrint = 1;
				nextPrintData = TOS;
				nextsp = sp_m1;
			end
			default: begin
				`ERROR(("NOT SUPPORTED"));
			end
		endcase
		if(opcode != CALL_FUNCTION && opcode != JUMP_ABSOLUTE) begin
			nextir = nextir + 2;
		end
	end
	
	always_ff @(posedge clk) begin
		if(reset) begin
			stack 		<= `SD 0;
			localStack	<= `SD 0;
			sp			<= `SD 63;
			localsp		<= `SD 0;
			bytecode 	<= `SD reset_bytecode;
			ir			<= `SD 0;
			codeObjects	<= `SD reset_codeObjects;
			frameStack[0].returnAddress <= `SD 0;
			frameStack[0].code <= `SD 0;
			frameStack[0].prevsp <= `SD 63;
			frameStack[`CALL_STACK_LIMIT-1:1] <= `SD 0;
			fp			<= `SD 0;
			consts		<= `SD reset_consts;
			globals 	<= `SD reset_globals;
			names 		<= `SD reset_names;
			strings		<= `SD reset_strings;
			listHeap 	<= `SD reset_listHeap;
			listStarts	<= `SD reset_listStarts;
			listSizes	<= `SD reset_listSizes;
			nextList	<= `SD reset_nextList;
			listHeapPtr	<= `SD reset_listHeapPtr;		
			error		<= `SD 0;
			halt 		<= `SD 0;
			doPrint 	<= `SD 0;
			printData	<= `SD 0;
		end else begin
			stack 		<= `SD nextStack;
			localStack 	<= `SD nextLocalStack;
			sp 			<= `SD nextsp;
			localsp		<= `SD nextLocalsp;
			ir 			<= `SD nextir;
			frameStack 	<= `SD nextFrameStack;
			fp 			<= `SD nextfp;
			globals		<= `SD nextglobals;
			names 		<= `SD nextnames;
			listHeap 	<= `SD nextListHeap;
			listStarts	<= `SD nextListStarts;
			listSizes	<= `SD nextListSizes;
			nextList	<= `SD nextNextList;
			listHeapPtr	<= `SD nextListHeapPtr;			
			error 		<= `SD nextError;
			halt		<= `SD nextHalt;
			doPrint 	<= `SD nextDoPrint;
			printData	<= `SD nextPrintData;
		end
	end

endmodule
